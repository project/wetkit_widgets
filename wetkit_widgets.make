; WetKit Widgets Makefile

api = 2
core = 7.x

projects[file_entity][version] = 2.37
projects[file_entity][subdir] = contrib
projects[file_entity][patch][2000934] = https://www.drupal.org/files/issues/2023-04-20/allow_selection_of-2000934-54.patch

projects[file_lock][version] = 2.0
projects[file_lock][subdir] = contrib

projects[media][version] = 2.30
projects[media][subdir] = contrib
